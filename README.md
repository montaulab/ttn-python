
Documentation
=============
[https://pypi.org/project/ttn/](https://pypi.org/project/ttn/)

Requirements
============

Python 3
--------

  * Install Python version 3
```
    sudo apt install python3
```
  * Install *ttn* library
```
    pip3 install ttn
```

Postgresql
----------

  * Install Postgresql database
```
    sudo apt install postgresql postgresql-contrib
```
  * Allow remote connection : */etc/postgresql/10/main/postgresql.conf*
```
    listen_addresses = '*'		# what IP address(es) to listen on;
```
  * Switch to user postgres and create database
```
    sudo -i -u postgres
    createdb lorawan;
```
  * Enter lorawan datbase
```
    psql lorawan
```
  * Create table for messages
```
    psql -c setup.sql
```

/etc/postgresql/10/main/pg_hba.conf
host    lorawan      all             all              md5
