CREATE TABLE messages (
id serial PRIMARY KEY,
date_time timestamptz DEFAULT NOW(),
payload varchar
);

CREATE USER lorawan;

GRANT SELECT, INSERT, UPDATE ON messages TO lorawan;

ALTER USER lorawan WITH ENCRYPTED PASSWORD 'LoRaWan';
