#!/usr/bin/env python3

import sys
import os
import configparser
#import psycopg2
import time
import ttn

conf = configparser.ConfigParser()
conf.read( os.path.join( os.getenv('HOME'), '.config/fetch-ttn/settings.ini' ) )

def uplink_callback(msg, client):
  print("\rReceived uplink from ", msg.dev_id)
  print(msg)

handler = ttn.HandlerClient(conf['TTN']['application_ID'], conf['TTN']['access_key'])

# using mqtt client
mqtt_client = handler.data()
mqtt_client.set_uplink_callback(uplink_callback)

print('Connecting')
mqtt_client.connect()
print('Waiting data')

try:
	while True:
		print( "\r|", end="" )
		time.sleep( 1 )
		print( "\r/", end="" )
		time.sleep( 1 )
		print( "\r-", end="" )
		time.sleep( 1 )
		print( "\r\\", end="" )
		time.sleep( 1 )
except (EOFError, KeyboardInterrupt) as e:
	print('\ndone')

mqtt_client.close()

